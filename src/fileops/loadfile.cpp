#include "loadfile.h"
#include <math/comp.h>

#include <fstream>

void load_f_cfloat(v_cfloat *signal, const std::string &p_filename) {
  std::streampos begin, end;
  std::ifstream myfile(p_filename, std::ios::binary);
  begin = myfile.tellg();
  myfile.seekg(0, std::ios::end);
  end = myfile.tellg();
  unsigned int file_len = (end - begin) / (2 * sizeof(float));
  std::cout << "size is: " << file_len << " cfloats.\n";

  signal->clear();
  signal->reserve(file_len);
  float real, imag;

  myfile.seekg(0, std::ios::beg);
  for (size_t i = 0; i < file_len; i++) {
    myfile.read(reinterpret_cast<char *>(&real), sizeof(real));
    myfile.read(reinterpret_cast<char *>(&imag), sizeof(imag));
    signal->push_back(cfloat(real, imag));
  }
  myfile.close();

  std::cout << " Max = " << max_abs(*signal) << " " << max_imag(*signal) << " "
            << max_real(*signal) << std::endl;
  std::cout << " Min = " << min_abs(*signal) << " " << min_imag(*signal) << " "
            << min_real(*signal) << std::endl;
  std::cout << " Len = " << signal->size() << std::endl;
}

void load_test_sig(v_cfloat *signal) {
  signal->push_back(cfloat(1, 3));
  signal->push_back(cfloat(2, 2));
  signal->push_back(cfloat(3, 1));
  signal->push_back(cfloat(2, 2));
  signal->push_back(cfloat(1, 3));

  std::cout << " Max = " << max_abs(*signal) << " " << max_imag(*signal) << " "
            << max_real(*signal) << std::endl;
  std::cout << " Min = " << min_abs(*signal) << " " << min_imag(*signal) << " "
            << min_real(*signal) << std::endl;
  std::cout << " Len = " << signal->size() << std::endl;
}
