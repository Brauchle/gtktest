#ifndef SRC_FILEOPS_LOADFILE_H
#define SRC_FILEOPS_LOADFILE_H

#include <iostream>
#include <sigproc/types.h>

void load_f_cfloat( v_cfloat *signal,const std::string& p_filename );
void load_test_sig( v_cfloat *signal);

#endif
