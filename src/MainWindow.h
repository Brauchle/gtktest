#ifndef SRC_MAINWINDOW_H
#define SRC_MAINWINDOW_H

#include "fileops/loadfile.h"
#include "plot/figure.h"

#include <gtkmm.h>

class MainWindow : public Gtk::ApplicationWindow
{
public:
  MainWindow();
  virtual ~MainWindow() = default;

  //SignalPlot m_plt;
  Figure m_fig_sig;
  Figure m_fig_fft;
  Figure m_fig_akf;
  Figure m_fig_pdf;
  Figure m_fig_check;

private:
  //Child widgets:
  Gtk::Box m_Box;

  Glib::RefPtr<Gtk::Builder> m_refBuilder;

  Gtk::Grid m_grid, m_grid_2, m_grid_fft_settings;
  Gtk::Box m_box_settings;
  Gtk::Frame m_frame, m_frame_fft_settings;
  Gtk::Notebook notebook_1;
  Gtk::Label m_Label1, m_Label2, m_Label3, m_Label4, m_Label5, m_label_fft_size;
  Gtk::Label m_Label_fs, m_Label_s;
  Gtk::Label m_Label_fs_v, m_Label_s_v;
  Gtk::SpinButton m_spinbutton_fft_size;
  Glib::RefPtr<Gtk::Adjustment> m_adjustment_fft_size;

  void on_spinbutton_fft_size();
};

#endif
