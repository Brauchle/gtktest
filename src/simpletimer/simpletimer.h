#ifndef _SRC_SIMPLETIMER_SIMPLETIMER
#define _SRC_SIMPLETIMER_SIMPLETIMER

#include <chrono>
#include <iostream>
#include <string>

template <typename T>
class SimpleTimer
{
public:
  void tik();
  void tok();
  void tok(std::string label);

private:
  std::chrono::high_resolution_clock::time_point start_time;

};

template <typename T> void SimpleTimer<T>::tik(){
  start_time = std::chrono::high_resolution_clock::now();
}

template <typename T> void SimpleTimer<T>::tok(){
  auto stop_time = std::chrono::high_resolution_clock::now();

  auto ms_time = std::chrono::duration_cast<T>(stop_time - start_time);
  std::cout << ms_time.count() << std::endl;
}

template <typename T> void SimpleTimer<T>::tok(std::string label){
  auto stop_time = std::chrono::high_resolution_clock::now();

  auto ms_time = std::chrono::duration_cast<T>(stop_time - start_time);
  std::cout << label << ms_time.count() << std::endl;
}

#endif
