#include "comp.h"

// Max Value
float max_abs(const v_cfloat& signal) {
  float max = abs(signal[0]);
  for (size_t i = 1; i < signal.size(); i++) {
    if (abs(signal[i]) > max) {
      max = abs(signal[i]);
    }
  }
  return max;
}

float max_real(const v_cfloat& signal) {
  float max = real(signal[0]);
  for (size_t i = 1; i < signal.size(); i++) {
    if (real(signal[i]) > max) {
      max = real(signal[i]);
    }
  }
  return max;
}

float max_imag(const v_cfloat& signal) {
  float max = imag(signal[0]);
  for (size_t i = 1; i < signal.size(); i++) {
    if (imag(signal[i]) > max) {
      max = imag(signal[i]);
    }
  }
  return max;
}

// Min Value
float min_abs(const v_cfloat& signal) {
  float min = abs(signal[0]);
  for (size_t i = 1; i < signal.size(); i++) {
    if (abs(signal[i]) < min) {
      min = abs(signal[i]);
    }
  }
  return min;
}

float min_real(const v_cfloat& signal) {
  float min = real(signal[0]);
  for (size_t i = 1; i < signal.size(); i++) {
    if (real(signal[i]) < min) {
      min = real(signal[i]);
    }
  }
  return min;
}

float min_imag(const v_cfloat& signal) {
  float min = imag(signal[0]);
  for (size_t i = 1; i < signal.size(); i++) {
    if (imag(signal[i]) < min) {
      min = imag(signal[i]);
    }
  }
  return min;
}
