#ifndef SRC_MATH_COMP_H
#define SRC_MATH_COMP_H

#include <sigproc/types.h>

float max_abs(const v_cfloat& signal);
float max_real(const v_cfloat& signal);
float max_imag(const v_cfloat& signal);

float min_abs(const v_cfloat& signal);
float min_real(const v_cfloat& signal);
float min_imag(const v_cfloat& signal);

#endif
