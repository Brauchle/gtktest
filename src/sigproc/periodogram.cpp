#include "periodogram.h"

v_cfloat periodogram(const v_cfloat sig, unsigned int fft_len){
  v_cfloat res;
  res.resize(fft_len);

  unsigned int parts = std::floor(sig.size()/fft_len);
  for (size_t i = 0; i < parts; i++) {
    v_cfloat temp = fft(sig, fft_len, i*fft_len);
    for (size_t j = 0; j < fft_len; j++) {
      res[j] += temp[j];
    }
  }

  return res;
}
