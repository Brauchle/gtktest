#include "correlation.h"

#include <iostream>

v_cfloat xcorr(const v_cfloat& signal1) {
  // Calculates the auto correlation of signal1
  int corr_len = signal1.size() * 2 - 1;
  v_cfloat v_temp(signal1.size() * 3 - 2, cfloat(0, 0));
  v_cfloat v_rest(corr_len, cfloat(0, 0));

  // ads padding to signal and calculates the conjugate complex outside the loop
  std::copy(signal1.begin(), signal1.end(), &v_temp[signal1.size() - 1]);
  for (cfloat& sample : v_temp) {
    sample = std::conj(sample);
  }

  // Calculate the correlation
  for (size_t k = 0; k < corr_len; k++) {
    for (size_t l = 0; l < signal1.size(); l++) {
      v_rest[k] = v_rest[k] + v_temp[k + l] * signal1[l];
    }
  }

  return v_rest;
};

v_cfloat xcorr(const v_cfloat& signal1, const v_cfloat& signal2) {
  // Calculates the cross correlation of signal1 and signal 2
  int corr_len = signal1.size() + signal2.size() - 1;
  v_cfloat v_temp(signal1.size() * 2 + signal2.size() - 2, cfloat(0, 0));
  v_cfloat v_rest(corr_len, cfloat(0, 0));

  // ads padding to signal 2 and calculates the conjugate complex outside the
  // loop
  std::copy(signal2.begin(), signal2.end(), &v_temp[signal1.size() - 1]);
  for (cfloat& sample : v_temp) {
    sample = std::conj(sample);
  }

  // Calculate the correlation
  for (size_t k = 0; k < corr_len; k++) {
    for (size_t l = 0; l < signal1.size(); l++) {
      v_rest[k] = v_rest[k] + signal1[l] * v_temp[k + l];
    }
  }

  return v_rest;
};
