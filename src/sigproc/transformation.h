#ifndef SRC_SIGPROC_TRANSFORMATION_H
#define SRC_SIGPROC_TRANSFORMATION_H

#include <sigproc/types.h>

enum Transformations {none, quadratic, cubic};

v_cfloat transform(const v_cfloat& signal, Transformations what);
v_cfloat pow(const v_cfloat& signal, const int& power);

#endif
