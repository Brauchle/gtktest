#include "convolution.h"

#include <iostream>

v_cfloat conv(const v_cfloat& signal1, const v_cfloat& signal2) {
  // Calculates the convolution of signal1 and signal 2
  int corr_len = signal1.size() + signal2.size() - 1;
  v_cfloat v_temp(signal2.size() * 2 + signal1.size() - 2, cfloat(0, 0));
  v_cfloat v_rest(corr_len, cfloat(0, 0));

  // ads padding to signal 1
  std::copy(signal1.begin(), signal1.end(), &v_temp[signal2.size() - 1]);

  // Calculate the convolution ! s2 gets inverted
  for (size_t k = 0; k < corr_len; k++) {
    for (size_t l = 0; l < signal2.size(); l++) {
      v_rest[k] = v_rest[k] + signal2[signal2.size() - l - 1] * v_temp[k + l];
    }
  }

  return v_rest;
};
