#ifndef SRC_SIGPROC_PERIODOGRAM_H
#define SRC_SIGPROC_PERIODOGRAM_H
// Tracks the angle of an object using the kalman filter

#include "types.h"
#include "fouriertransformation.h"

v_cfloat periodogram(const v_cfloat sig, unsigned int fft_len);

#endif
