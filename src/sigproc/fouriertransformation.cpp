#include "fouriertransformation.h"
#include <iostream>

// FFT
////////////////////////////////////////////////////////////////////////////////
v_cfloat fft(const v_cfloat& sig) {
  unsigned int len = sig.size();

  if( (len & (len - 1)) == 0 ){
    v_cfloat res = fft_calc(sig, sig.size(), 0, 0);
    return res;
  }
  else{
    std::cout << "FFT needs the signal size to be a power of 2" << std::endl;
    v_cfloat res;
    res.resize(2);
    return res;
  }
}

v_cfloat fft(const v_cfloat& sig, unsigned int fft_len) {
  // Check for Power of 2
  if( (fft_len & (fft_len - 1)) != 0 ){
    std::cout << "FFT needs the signal size to be a power of 2" << std::endl;
    v_cfloat res;
    res.resize(2);
    return res;
  }

  // Check if padding is necessary
  if( fft_len <= sig.size() ){
    v_cfloat res = fft_calc(sig, sig.size(), 0, 0);
    return res;
  }
  else{
    v_cfloat padded_signal;
    padded_signal.resize(fft_len);
    for (size_t i = 0; i < sig.size(); i++) {
      padded_signal[i] = sig[i];
    }
    v_cfloat res = fft_calc(padded_signal, fft_len, 0, 0);
    return res;
  }
}

v_cfloat fft(const v_cfloat& sig, unsigned int fft_len, unsigned int start) {
  // Check for Power of 2
  if( (fft_len & (fft_len - 1)) != 0 ){
    std::cout << "FFT needs the signal size to be a power of 2" << std::endl;
    v_cfloat res;
    res.resize(2);
    return res;
  }

  // Check if padding is necessary
  if( (start + fft_len) <= sig.size() ){
    v_cfloat res = fft_calc(sig, sig.size(), start, 0);
    return res;
  }
  else{
    v_cfloat padded_signal;
    padded_signal.resize(start + fft_len);
    for (size_t i = 0; i < sig.size(); i++) {
      padded_signal[i] = sig[i];
    }
    v_cfloat res = fft_calc(padded_signal, fft_len, start, 0);
    return res;
  }
}

v_cfloat fft_calc(const v_cfloat& sig, unsigned int fft_len, unsigned int start, unsigned int level) {
  // -inf +inf int sig(t) exp -i 2 pi f t dt
  unsigned int teiler = pow(2, level);
  unsigned int len = (fft_len / teiler);
  unsigned int len_2 = len / 2;

  v_cfloat res;
  res.resize(len); // PERFORMANCE PROBLEM IT INITIALIZES ALL THE FUCKING VALUES

  cfloat wnk(0, 0);
  cfloat w_n(0, -2 * M_PI / len);

  if ( len_2 != 1){
    v_cfloat a = fft_calc(sig, fft_len, start + 0, level + 1);
    v_cfloat b = fft_calc(sig, fft_len, start + teiler, level + 1);

    for (unsigned int k = 0; k < len_2; k++) {
      wnk = std::exp(w_n * cfloat(k,0));
      res[k]         = a[k] + wnk * b[k]; // 2m
      res[k + len_2] = a[k] - wnk * b[k]; // 2m
    }
  }
  else{
    res[0] = sig[start] + sig[start + teiler];
    res[1] = sig[start] - sig[start + teiler]; // 2m
  }

  return res;
}

void fft_shift(v_cfloat *sig){
  v_cfloat tmp;
  unsigned int len, n_len;
  len = sig->size();
  len = std::floor(len / 2 + 1);
  n_len = sig->size() - len;

  // Speichern der len ersten Werte und
  tmp.resize(len);
  for (size_t i = 0; i < len; i++) {
    tmp[i] = sig->at(i);
  }
  // verschieben der negativen Werte
  for (size_t i = 0; i < n_len; i++) {
    sig->at(i) = sig->at(len + i);
  }
  // Verschieben der Positiven Werte aus TMP
  for (size_t i = 0; i < len; i++) {
    sig->at(n_len + i) = tmp[i];
  }
}

// IFFT
////////////////////////////////////////////////////////////////////////////////
v_cfloat ifft(const v_cfloat& sig) {
  unsigned int len = sig.size();

  if( (len & (len - 1)) == 0 ){
    v_cfloat res = ifft_calc(sig, sig.size(), 0, 0);
    cfloat fac = cfloat(1.0/len, 0);
    for (cfloat& e : res) {
      e = fac * e;
    }
    return res;
  }
  else{
    std::cout << "IFFT needs the signal size to be a power of 2" << std::endl;
    v_cfloat res;
    res.resize(2);
    return res;
  }
}

v_cfloat ifft(const v_cfloat& sig, unsigned int fft_len) {
  // Check for Power of 2
  if( (fft_len & (fft_len - 1)) != 0 ){
    std::cout << "FFT needs the signal size to be a power of 2" << std::endl;
    v_cfloat res;
    res.resize(2);
    return res;
  }

  // Check if padding is necessary
  if( fft_len <= sig.size() ){
    v_cfloat res = ifft_calc(sig, sig.size(), 0, 0);
    cfloat fac = cfloat(1.0/fft_len, 0);
    for (cfloat& e : res) {
      e = fac * e;
    }
    return res;
  }
  else{
    v_cfloat padded_signal;
    padded_signal.resize(fft_len);
    for (size_t i = 0; i < sig.size(); i++) {
      padded_signal[i] = sig[i];
    }
    v_cfloat res = ifft_calc(padded_signal, fft_len, 0, 0);
    cfloat fac = cfloat(1.0/fft_len, 0);
    for (cfloat& e : res) {
      e = fac * e;
    }
    return res;
  }
}

v_cfloat ifft(const v_cfloat& sig, unsigned int fft_len, unsigned int start) {
  // Check for Power of 2
  if( (fft_len & (fft_len - 1)) != 0 ){
    std::cout << "FFT needs the signal size to be a power of 2" << std::endl;
    v_cfloat res;
    res.resize(2);
    return res;
  }

  // Check if padding is necessary
  if( (start + fft_len) <= sig.size() ){
    v_cfloat res = ifft_calc(sig, sig.size(), start, 0);
    cfloat fac = cfloat(1.0/fft_len, 0);
    for (cfloat& e : res) {
      e = fac * e;
    }
    return res;
  }
  else{
    v_cfloat padded_signal;
    padded_signal.resize(start + fft_len);
    for (size_t i = 0; i < sig.size(); i++) {
      padded_signal[i] = sig[i];
    }
    v_cfloat res = ifft_calc(padded_signal, fft_len, start, 0);
    cfloat fac = cfloat(1.0/fft_len, 0);
    for (cfloat& e : res) {
      e = fac * e;
    }
    return res;
  }
}

v_cfloat ifft_calc(const v_cfloat& sig, unsigned int fft_len, unsigned int start, unsigned int level) {
  // -inf +inf int sig(t) exp -i 2 pi f t dt
  unsigned int teiler = pow(2, level);
  unsigned int len = (fft_len / teiler);
  unsigned int len_2 = len / 2;

  v_cfloat res;
  res.resize(len); // PERFORMANCE PROBLEM IT INITIALIZES ALL THE FUCKING VALUES

  cfloat wnk(0, 0);
  cfloat w_n(0, 2 * M_PI / len);

  if ( len_2 != 1){
    v_cfloat a = fft_calc(sig, fft_len, start + 0, level + 1);
    v_cfloat b = fft_calc(sig, fft_len, start + teiler, level + 1);

    for (unsigned int k = 0; k < len_2; k++) {
      wnk = std::exp(w_n * cfloat(k,0));
      res[k]         = a[k] + wnk * b[k]; // 2m
      res[k + len_2] = a[k] - wnk * b[k]; // 2m
    }
  }
  else{
    res[0] = sig[start] + sig[start + teiler];
    res[1] = sig[start] - sig[start + teiler]; // 2m
  }

  return res;
}

// Slow and Steady Fourier Transform
////////////////////////////////////////////////////////////////////////////////
v_cfloat ft(const v_cfloat& sig) {
  // -inf +inf int sig(t) exp -i 2 pi f t dt
  v_cfloat res;
  cfloat temp(0, 0);
  cfloat expon(0, -2 * M_PI / sig.size());
  res.reserve(sig.size());

  for (int f = 0; f < sig.size(); f++) {
    /* frequencies from 0 to N (fs), works because of cyclic properties */
    temp = 0;
    for (int t = 0; t < sig.size(); t++) {
      /* integrate over t */
      temp += sig[t] * std::exp(expon * cfloat(f, 0) * cfloat(t, 0));
    }
    res.push_back(temp);
  }
  return res;
}

v_cfloat ift(const v_cfloat& sig) {
  // -inf +inf int sig(t) exp -i 2 pi f t dt
  v_cfloat res;
  cfloat temp(0, 0);
  cfloat expon(0, 2 * M_PI / sig.size());

  for (int f = 0; f < sig.size(); f++) {
    /* frequencies from 0 to N (fs), works because of cyclic properties */
    temp = 0;
    for (int t = 0; t < sig.size(); t++) {
      /* integrate over t */
      temp += sig[t] * std::exp(expon * cfloat(f, 0) * cfloat(t, 0));
    }
    res.push_back(temp);
  }
  return res;
}
