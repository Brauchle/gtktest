#ifndef SRC_SIGPROC_FREQUENCYSHIFT_H
#define SRC_SIGPROC_FREQUENCYSHIFT_H
// Tracks the angle of an object using the kalman filter

#include "types.h"

void f_shift(v_cfloat *sig, float freq); // fractional frequency
void f_shift(v_cfloat *sig, float freq, float f_s); // frequency + sample rate

#endif
