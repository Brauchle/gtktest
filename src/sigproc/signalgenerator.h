#ifndef SRC_SIGPROC_SIGNALGENERATOR_H
#define SRC_SIGPROC_SIGNALGENERATOR_H
// Tracks the angle of an object using the kalman filter

#include "types.h"

v_cfloat gen_cos(float freq, float fs, int samples);
v_cfloat gen_step(int samples, int step);

#endif
