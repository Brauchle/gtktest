#include "frequencyshift.h"

void f_shift(v_cfloat *sig, float freq) {
  // -inf +inf int sig(t) exp -i 2 pi f t dt
  cfloat expon(0, -1 * freq * 2 * M_PI);

  for (int f = 0; f < sig->size(); f++) {
    sig->at(f) = sig->at(f) * std::exp(expon * cfloat(f, 0));
  }
}

void f_shift(v_cfloat *sig, float freq, float f_s) {
  // -inf +inf int sig(t) exp -i 2 pi f t dt
  cfloat expon(0, -2 * M_PI * freq / f_s);

  for (int f = 0; f < sig->size(); f++) {
    sig->at(f) = sig->at(f) * std::exp(expon * cfloat(f, 0));
  }
}
