#include "signalgenerator.h"

v_cfloat gen_cos(float freq, float fs, int samples) {
  v_cfloat tmp;
  tmp.reserve(samples);
  cfloat fac(2 * M_PI * freq / fs, 0);

  for (unsigned int i = 0; i < samples; i++) {
    tmp.push_back(std::cos(fac * cfloat(i, 0)));
  }

  return tmp;
}

v_cfloat gen_step(int samples, int step){
  v_cfloat tmp;
  tmp.resize(samples);
  for (size_t i = 0; i < samples; i++) {
    if( i > (samples-step)) {
      tmp[i] = cfloat(1,0);
    }
  }
  return tmp;
}
