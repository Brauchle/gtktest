#ifndef SRC_SIGPROC_CORRELATION_H
#define SRC_SIGPROC_CORRELATION_H
// Tracks the angle of an object using the kalman filter

#include "types.h"

v_cfloat xcorr(const v_cfloat& signal1);
v_cfloat xcorr(const v_cfloat& signal1, const v_cfloat& signal2);

#endif
