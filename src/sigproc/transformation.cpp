#include "transformation.h"
#include "fouriertransformation.h"

#include <iostream>

v_cfloat transform(const v_cfloat& signal, Transformations what) {
  switch (what) {
    case none:
      std::cout << "none" << std::endl;
      return signal;
      break;
    case quadratic:
      std::cout << "quadratic" << std::endl;
      return pow(signal, 2);
      break;
    case cubic:
      std::cout << "cubic" << std::endl;
      return pow(signal, 4);
      break;
    default:
      return signal;
  }
}

v_cfloat pow(const v_cfloat& signal, const int& power) {
  v_cfloat tmp;
  tmp.reserve(signal.size());
  for (auto e : signal) {
    tmp.push_back(std::pow(e, power));
  }
  return tmp;
}
