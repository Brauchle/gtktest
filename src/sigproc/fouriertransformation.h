#ifndef SRC_SIGPROC_FOURIERTRANSFORMATION_H
#define SRC_SIGPROC_FOURIERTRANSFORMATION_H
// Tracks the angle of an object using the kalman filter

#include "types.h"

v_cfloat ft(const v_cfloat& sig);
v_cfloat ift(const v_cfloat& sig);

v_cfloat fft(const v_cfloat& sig);
v_cfloat fft(const v_cfloat& sig, unsigned int fft_len);
v_cfloat fft(const v_cfloat& sig, unsigned int fft_len, unsigned int start);
v_cfloat fft_calc(const v_cfloat& sig, unsigned int fft_len, unsigned int start, unsigned int level);

v_cfloat ifft(const v_cfloat& sig);
v_cfloat ifft(const v_cfloat& sig, unsigned int fft_len);
v_cfloat ifft(const v_cfloat& sig, unsigned int fft_len, unsigned int start);
v_cfloat ifft_calc(const v_cfloat& sig, unsigned int fft_len, unsigned int start, unsigned int level);

void fft_shift(v_cfloat *sig);


#endif
