#ifndef SRC_SIGPROC_TYPES_H
#define SRC_SIGPROC_TYPES_H
// Tracks the angle of an object using the kalman filter

#include <complex>
#include <vector>

using cfloat   = std::complex<float>;
using v_cfloat = std::vector<cfloat>;

#endif
