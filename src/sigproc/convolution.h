#ifndef SRC_SIGPROC_CONVOLUTION_H
#define SRC_SIGPROC_CONVOLUTION_H
// Tracks the angle of an object using the kalman filter

#include "types.h"

v_cfloat conv(const v_cfloat& signal1, const v_cfloat& signal2);

#endif
