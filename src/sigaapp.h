#ifndef SRC_SIGAAPP_H
#define SRC_SIGAAPP_H

#include "sigproc/signalgenerator.h"
#include "sigproc/transformation.h"
#include "sigproc/periodogram.h"
#include "sigproc/types.h"

#include <gtkmm.h>


class SigA : public Gtk::Application
{
protected:
  SigA();

public:
  static Glib::RefPtr<SigA> create();

protected:
  //Overrides of default signal handlers:
  void on_startup() override;
  void on_activate() override;

private:
  void create_window();

  void on_window_hide(Gtk::Window* window);
  void on_menu_file_new_generic();
  void on_menu_file_quit();
  void on_menu_help_about();

  void on_menu_file_open();
  void on_menu_file_save();

  void on_menu_generate_signal();

  void on_function_transform(Transformations);
  void on_function_transform_none();
  void on_function_transform_quadratic();
  void on_function_transform_cubic();

  void calc_stats();

  // Signal Properties
  float        sample_rate;
  unsigned int samples;

  v_cfloat signal;
  v_cfloat trans_sig;
  v_cfloat trans_fft;
  v_cfloat trans_akf;
  v_cfloat trans_pdf;

  v_cfloat trans_check;

  Glib::RefPtr<Gtk::Builder> m_refBuilder;
};

#endif
