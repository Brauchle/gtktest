#include "sigaapp.h"
#include "MainWindow.h"
#include "sigproc/correlation.h"
#include "sigproc/fouriertransformation.h"
#include "simpletimer/simpletimer.h"

#include <iostream>

SigA::SigA() : Gtk::Application("no.domain.signal_analyzer") {
  Glib::set_application_name("Signal Analyzer");
}

Glib::RefPtr<SigA> SigA::create() { return Glib::RefPtr<SigA>(new SigA()); }

void SigA::on_startup() {
  // Call the base class's implementation:
  Gtk::Application::on_startup();

  // Create actions for menus and toolbars.
  add_action("quit", sigc::mem_fun(*this, &SigA::on_menu_file_quit));

  // File menu:
  add_action("open", sigc::mem_fun(*this, &SigA::on_menu_file_open));
  add_action("save", sigc::mem_fun(*this, &SigA::on_menu_file_save));

  add_action("generate", sigc::mem_fun(*this, &SigA::on_menu_generate_signal));

  // Help menu:
  add_action("about", sigc::mem_fun(*this, &SigA::on_menu_help_about));

  // Transform Menu
  add_action("transform_none", sigc::mem_fun(*this, &SigA::on_function_transform_none));
  add_action("transform_quadratic", sigc::mem_fun(*this, &SigA::on_function_transform_quadratic));
  add_action("transform_cubic", sigc::mem_fun(*this, &SigA::on_function_transform_cubic));

  // Settings menu

  m_refBuilder = Gtk::Builder::create();

  // Layout the actions in a menubar and an application menu:
  Glib::ustring ui_info =
      "<interface>"
      "  <!-- menubar -->"
      "  <menu id='menu-example'>"
      "    <submenu>"
      "      <attribute name='label' translatable='yes'>_File</attribute>"
      "      <section>"
      "        <item>"
      "          <attribute name='label' translatable='yes'>_Open</attribute>"
      "          <attribute name='action'>app.open</attribute>"
      "          <attribute name='accel'>&lt;Primary&gt;o</attribute>"
      "        </item>"
      "        <item>"
      "          <attribute name='label' translatable='yes'>_Save</attribute>"
      "          <attribute name='action'>app.save</attribute>"
      "          <attribute name='accel'>&lt;Primary&gt;s</attribute>"
      "        </item>"
      "      </section>"
      "      <section>"
      "        <item>"
      "          <attribute name='label' translatable='yes'>_Generate "
      "Signal</attribute>"
      "          <attribute name='action'>app.generate</attribute>"
      "          <attribute name='accel'>&lt;Primary&gt;q</attribute>"
      "        </item>"
      "      </section>"
      "      <section>"
      "        <item>"
      "          <attribute name='label' translatable='yes'>_Quit</attribute>"
      "          <attribute name='action'>app.quit</attribute>"
      "          <attribute name='accel'>&lt;Primary&gt;q</attribute>"
      "        </item>"
      "      </section>"
      "    </submenu>"
      "    <submenu>"
      "      <attribute name='label' translatable='yes'>_Edit</attribute>"
      "      <section>"
      "        <item>"
      "          <attribute name='label' translatable='yes'>_Undo</attribute>"
      "          <attribute name='action'>app.about</attribute>"
      "        </item>"
      "        <item>"
      "          <attribute name='label' translatable='yes'>_Redo</attribute>"
      "          <attribute name='action'>app.about</attribute>"
      "        </item>"
      "      </section>"
      "      <section>"
      "        <submenu>"
      "         <attribute name='label' translatable='yes'>Transform</attribute>"
      "         <section>"
      "         <item>"
      "           <attribute name='label' translatable='yes'>None</attribute>"
      "           <attribute name='action'>app.transform_none</attribute>"
      "           <attribute name='accel'>&lt;Primary&gt;o</attribute>"
      "         </item>"
      "         <item>"
      "           <attribute name='label' translatable='yes'>Quadratic</attribute>"
      "           <attribute name='action'>app.transform_quadratic</attribute>"
      "           <attribute name='accel'>&lt;Primary&gt;o</attribute>"
      "         </item>"
      "         <item>"
      "           <attribute name='label' translatable='yes'>Cubic</attribute>"
      "           <attribute name='action'>app.transform_cubic</attribute>"
      "           <attribute name='accel'>&lt;Primary&gt;o</attribute>"
      "         </item>"
      "         </section>"
      "        </submenu>"
      "      </section>"
      ""
      "      <section>"
      "        <item>"
      "          <attribute name='label' "
      "translatable='yes'>_Resample</attribute>"
      "          <attribute name='action'>app.about</attribute>"
      "        </item>"
      "        <item>"
      "          <attribute name='label' translatable='yes'>_Shift "
      "Frequency</attribute>"
      "          <attribute name='action'>app.about</attribute>"
      "        </item>"
      "        <item>"
      "          <attribute name='label' translatable='yes'>_Filter</attribute>"
      "          <attribute name='action'>app.about</attribute>"
      "        </item>"
      "        <item>"
      "          <attribute name='label' "
      "translatable='yes'>_Normalize</attribute>"
      "          <attribute name='action'>app.about</attribute>"
      "        </item>"
      "      </section>"
      "    </submenu>"
      "    <submenu>"
      "      <attribute name='label' translatable='yes'>_Help</attribute>"
      "      <section>"
      "        <item>"
      "          <attribute name='label' translatable='yes'>_About</attribute>"
      "          <attribute name='action'>app.about</attribute>"
      "        </item>"
      "      </section>"
      "    </submenu>"
      "  </menu>";

  try {
    m_refBuilder->add_from_string(ui_info);
  } catch (const Glib::Error& ex) {
    std::cerr << "Building menus failed: " << ex.what();
  }

  // Get the menubar and the app menu, and add them to the application:
  auto object = m_refBuilder->get_object("menu-example");
  auto gmenu = Glib::RefPtr<Gio::Menu>::cast_dynamic(object);
  if (!(gmenu)) {
    g_warning("GMenu not found");
  } else {
    set_menubar(gmenu);
  }
}

void SigA::on_activate() {
  // std::cout << "debug1: " << G_STRFUNC << std::endl;
  // The application has been started, so let's show a window.
  // A real application might want to reuse this window in on_open(),
  // when asked to open a file, if no changes have been made yet.
  create_window();
}

void SigA::create_window() {
  auto win = new MainWindow();

  // Make sure that the application runs for as long this window is still open:
  add_window(*win);

  // Delete the window when it is hidden.
  // That's enough for this simple example.
  win->signal_hide().connect(sigc::bind<Gtk::Window*>(
      sigc::mem_fun(*this, &SigA::on_window_hide), win));

  win->show_all();
}

void SigA::on_window_hide(Gtk::Window* window) { delete window; }

void SigA::on_menu_file_new_generic() {
  std::cout << "A File|New menu item was selected." << std::endl;
}

void SigA::on_menu_file_quit() {
  std::cout << G_STRFUNC << std::endl;
  quit();  // Not really necessary, when Gtk::Widget::hide() is called.

  // Gio::Application::quit() will make Gio::Application::run() return,
  // but it's a crude way of ending the program. The window is not removed
  // from the application. Neither the window's nor the application's
  // destructors will be called, because there will be remaining reference
  // counts in both of them. If we want the destructors to be called, we
  // must remove the window from the application. One way of doing this
  // is to hide the window.
  std::vector<Gtk::Window*> windows = get_windows();
  if (!windows.empty()) {
    windows[0]->hide();  // In this simple case, we know there is only one window.
  }
}

void SigA::on_menu_help_about() {
  std::cout << "App|Help|About was selected." << std::endl;
}

void SigA::on_menu_file_open() {
  Gtk::FileChooserDialog dialog("Please choose a file",
                                Gtk::FILE_CHOOSER_ACTION_OPEN);
  // dialog.set_transient_for(*this);

  // Add response buttons the the dialog:
  dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
  dialog.add_button("_Open", Gtk::RESPONSE_OK);

  // Show the dialog and wait for a user response:
  int result = dialog.run();

  // Handle the response:
  switch (result) {
    case (Gtk::RESPONSE_OK): {
      std::cout << "Open clicked." << std::endl;

      // Notice that this is a std::string, not a Glib::ustring.
      std::string filename = dialog.get_filename();
      std::cout << "File selected: " << filename << std::endl;

      load_f_cfloat(&signal, filename);
      on_function_transform(none);

      break;
    }
    case (Gtk::RESPONSE_CANCEL): {
      std::cout << "Cancel clicked." << std::endl;
      break;
    }
    default: {
      std::cout << "Unexpected button clicked." << std::endl;
      break;
    }
  }
}

void SigA::on_menu_generate_signal() {
  signal.clear();
  // signal = gen_cos(8, 128, 1024);
  signal = gen_step(1024, 32);
  on_function_transform(none);
}

void SigA::on_menu_file_save() {
  std::cout << "File almost saved." << std::endl;
}

void SigA::on_function_transform_none(){
  on_function_transform(none);
}

void SigA::on_function_transform_quadratic(){
  on_function_transform(quadratic);
}

void SigA::on_function_transform_cubic(){
  on_function_transform(cubic);
}

void SigA::on_function_transform(Transformations trx) {
  SimpleTimer<std::chrono::milliseconds> timer;

  timer.tik();
  trans_sig.clear();
  trans_pdf.clear();
  trans_akf.clear();
  trans_fft.clear();
  timer.tok(" clear : ");

  trans_check.clear();

  timer.tik();
  trans_sig = transform(signal, trx);
  timer.tok(" transform : ");

  timer.tik();
  trans_fft = fft(trans_sig);
  fft_shift(&trans_fft);
  timer.tok(" ft : ");

  timer.tik();
  trans_check = fft(trans_sig, 1024);
  fft_shift(&trans_check);
  timer.tok(" fft : ");

  timer.tik();
  trans_akf = xcorr(trans_sig);
  timer.tok(" xcorr : ");

  timer.tik();
  trans_pdf = periodogram(trans_akf, 1024);
  fft_shift(&trans_pdf);
  timer.tok(" ft akf : ");

  cfloat sumtest;
  for (size_t i = 0; i < trans_fft.size(); i++) {
    /* code */
    sumtest += (trans_fft[i] - trans_check[i]);
  }
  std::cout << std::endl << sumtest << std::endl;

  MainWindow* appwindow = nullptr;
  auto windows = get_windows();
  if (!windows.empty()) {
    appwindow = dynamic_cast<MainWindow*>(windows[0]);

    appwindow->m_fig_sig.plot(trans_sig);
    appwindow->m_fig_fft.plot(trans_fft);
    appwindow->m_fig_pdf.plot(trans_pdf);
    appwindow->m_fig_akf.plot(trans_akf);
    appwindow->m_fig_check.plot(trans_check);
  }
}
