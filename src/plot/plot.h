#ifndef SRC_PLOT_PLOT_H
#define SRC_PLOT_PLOT_H

#include <sigproc/types.h>

#include <cairomm/context.h>
#include <gtkmm.h>

enum ePlotTools {pointer, zoom};

class Plot : public Gtk::DrawingArea
{
public:
  Plot();
  virtual ~Plot() = default;

  void set_x_values(const v_cfloat& p_x_vals);
  void set_y_values(const v_cfloat& p_y_vals);
  void set_tool(ePlotTools r){m_tool = r;}
  void reset_zoom();

private:
  // Mouse Input
  ePlotTools m_tool = pointer;

  // Zoom
  float z_start, z_stop;
  bool zoom_first_clk = false;
  void update_zoom();

  float zm_s=0, zm_e=1;


  // transform
  int m_padding;
  int m_height, m_width;
  float min_x, min_y, max_x, max_y;

  // Options
  bool has_x_values;
  bool has_y_values;

  float x_s, x_e, y_s, y_e;

  v_cfloat x_values, y_values;

  // drawing functions
  void draw_axis(const Cairo::RefPtr<Cairo::Context>& cr);
  void draw_signal(const Cairo::RefPtr<Cairo::Context>& cr);
  // Coordinate Transformation
  float prj_x(const float& xv, const float& mx, const float& bx);
  float prj_y(const float& yv, const float& my, const float& by);
protected:
  //Override default signal handler:
  bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;
  bool on_button_press_event(GdkEventButton *event) override;
};

#endif
