#include "plot.h"
#include <math/comp.h>

#include <iostream>

Plot::Plot() {
  // Enable Mouse
  add_events(Gdk::BUTTON_PRESS_MASK);

  has_x_values = false;
  has_y_values = false;
}

bool Plot::on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {
  Gtk::Allocation allocation = get_allocation();
  m_width = allocation.get_width();
  m_height = allocation.get_height();
  // Fill
  cr->set_source_rgb(1, 1, 1);
  cr->rectangle(0, 0, m_width, m_height);
  cr->fill();

  // Padding
  m_padding = 40;
  m_width = m_width - 2 * m_padding;
  m_height = m_height - 2 * m_padding;

  // Draw Coordinate System
  draw_axis(cr);

  return true;
}

bool Plot::on_button_press_event(GdkEventButton* event) {
  switch (m_tool) {
    case pointer:
      std::cout << "Does Nothing Yet" << event->x << '\n';
      break;
    case zoom:
      if ((event->type == GDK_BUTTON_PRESS) && (event->button == 1)) {
        if (!zoom_first_clk) {
          z_start = event->x;
          zoom_first_clk = true;
        } else {
          z_stop = event->x;
          if (z_stop < z_start) {
            float temp = z_start;
            z_start = z_stop;
            z_stop  = z_start;
          }
          zoom_first_clk = false;
          update_zoom();
          queue_draw();
        }
      }

      if ((event->type == GDK_BUTTON_PRESS) && (event->button == 3)) {
        zoom_first_clk = false;
      }
      break;
  }

  return true;
}

void Plot::update_zoom() {

  float z_ts = (z_start - m_padding) / m_width;
  float z_te = (z_stop - m_padding) / m_width;
  float z_or = zm_e - zm_s;
  zm_e = zm_s + z_or * z_te;
  zm_s = zm_s + z_or * z_ts;

}

void Plot::reset_zoom() {
  zm_s = 0;
  zm_e = 1;
  queue_draw();
}

void Plot::draw_axis(const Cairo::RefPtr<Cairo::Context>& cr) {
  cr->set_line_width(2.0);
  cr->set_source_rgb(0, 0, 0);

  cr->move_to(m_padding, m_padding);
  cr->line_to(m_padding, m_padding + m_height);
  cr->line_to(m_padding + m_width, m_padding + m_height);
  cr->stroke();

  int as = 10;
  cr->move_to(m_padding, m_padding);
  cr->rel_line_to((as / 2.0), as);
  cr->rel_line_to(-as, 0);
  cr->line_to(m_padding, m_padding);
  cr->fill();

  cr->move_to(m_padding + m_width, m_padding + m_height);
  cr->rel_line_to(-as, (as / 2.0));
  cr->rel_line_to(0, -as);
  cr->rel_line_to(as, (as / 2.0));
  cr->fill();

  if (has_y_values) {
    float min_x, min_y, max_x, max_y;
    max_y = max_abs(y_values);
    min_y = -1 * max_y; // min_abs(y_values);
    min_x = std::floor(zm_s * y_values.size());
    max_x = std::ceil(zm_e * y_values.size());

    float n_samples = max_x - min_x;

    float mx, my, bx, by;
    mx = m_width / n_samples;
    bx = -1 * min_x;

    my = m_height / (max_y - min_y);
    by = -1 * min_y;

    // DASHES
    cr->set_line_width(1.0);
    std::vector<double> dashes;
    dashes.push_back(10.0);
    dashes.push_back(5.0);
    cr->set_dash(dashes, 0);

    cr->set_source_rgb(.4, .4, .4);
    cr->move_to(prj_x(min_x, mx, bx), prj_y(std::abs(y_values[min_x]), my, by));
    for (size_t i = std::floor(min_x) + 1; i < std::ceil(max_x); i++) {
      cr->line_to(prj_x(i, mx, bx), prj_y(std::abs(y_values[i]), my, by));
    }
    cr->stroke();

    cr->set_line_width(2.0);
    dashes.clear();
    cr->set_dash(dashes, 0);

    cr->set_source_rgb(0, 0, 1);
    cr->move_to(prj_x(min_x, mx, bx), prj_y(std::real(y_values[min_x]), my, by));
    for (size_t i = std::floor(min_x) + 1; i < std::ceil(max_x); i++) {
      cr->line_to(prj_x(i, mx, bx), prj_y(std::real(y_values[i]), my, by));
    }
    cr->stroke();

    cr->set_source_rgb(1, 0, 0);
    cr->move_to(prj_x(min_x, mx, bx), prj_y(std::imag(y_values[min_x]), my, by));
    for (size_t i = std::floor(min_x) + 1; i < std::ceil(max_x); i++) {
      cr->line_to(prj_x(i, mx, bx), prj_y(std::imag(y_values[i]), my, by));
    }
    cr->stroke();
  }
}

float Plot::prj_x(const float& xv, const float& mx, const float& bx) {
  return m_padding + mx * (xv + bx);
}

float Plot::prj_y(const float& yv, const float& my, const float& by) {
  return m_padding + m_height - my * (yv + by);
}

void Plot::set_x_values(const v_cfloat& p_x_vals) {
  x_values = p_x_vals;
  has_x_values = true;
}

void Plot::set_y_values(const v_cfloat& p_y_vals) {
  y_values = p_y_vals;
  has_y_values = true;
}
