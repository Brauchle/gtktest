#ifndef SRC_PLOT_FIGURE_H
#define SRC_PLOT_FIGURE_H

#include "plot/plot.h"
#include <sigproc/types.h>

#include <gtkmm.h>

class Figure : public Gtk::Grid
{
public:
  Figure();
  ~Figure() = default;

  void plot(const v_cfloat& signal);
  //void plot(v_cfloat, v_cfloat);
  //void plot(v_cfloat, v_cfloat, float, float);

private:
  Plot m_plt;

  //Signal handlers:
  void on_toolbar_default();
  void on_toolbar_reset();
  void on_toolbar_zoom();

  Glib::RefPtr<Gtk::Builder> m_refBuilder;
  Glib::RefPtr<Gio::SimpleActionGroup> m_refActionGroup;
};

#endif
