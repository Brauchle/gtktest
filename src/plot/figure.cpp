#include "figure.h"

#include <iostream>

Figure::Figure() {
  // actions
  m_refActionGroup = Gio::SimpleActionGroup::create();

  // m_refActionGroup->add_action("reload", sigc::mem_fun(*this,
  // &Figure::on_f_toolbar_refresh)); m_refActionGroup->add_action("zoom",
  // sigc::mem_fun(*this, &Figure::on_f_toolbar_zoom));
  m_refActionGroup->add_action(
      "default", sigc::mem_fun(*this, &Figure::on_toolbar_default));
  m_refActionGroup->add_action("reset",
                               sigc::mem_fun(*this, &Figure::on_toolbar_reset));
  m_refActionGroup->add_action("zoom",
                               sigc::mem_fun(*this, &Figure::on_toolbar_zoom));

  insert_action_group("figure", m_refActionGroup);

  // Create the toolbar and add it to a container widget:
  m_refBuilder = Gtk::Builder::create();

  Glib::ustring ui_info =
      "<!-- Generated with glade 3.18.3 -->"
      "<interface>"
      "  <requires lib='gtk+' version='3.4'/>"
      "  <object class='GtkToolbar' id='f_toolbar'>"
      "    <property name='visible'>True</property>"
      "    <property name='can_focus'>False</property>"
      "    <child>"
      "      <object class='GtkToolButton' id='f_toolbar_mouse'>"
      "        <property name='visible'>True</property>"
      "        <property name='can_focus'>False</property>"
      "        <property name='tooltip_text' translatable='yes'>Zoom</property>"
      "        <property name='action_name'>figure.default</property>"
      "        <property name='icon_name'>edit-select</property>"
      "      </object>"
      "      <packing>"
      "        <property name='expand'>False</property>"
      "        <property name='homogeneous'>True</property>"
      "      </packing>"
      "    </child>"
      "    <child>"
      "      <object class='GtkToolButton' id='f_toolbar_reload'>"
      "        <property name='visible'>True</property>"
      "        <property name='can_focus'>False</property>"
      "        <property name='tooltip_text' "
      "translatable='yes'>Reload</property>"
      "        <property name='action_name'>figure.reset</property>"
      "        <property name='icon_name'>zoom-original</property>"
      "      </object>"
      "      <packing>"
      "        <property name='expand'>False</property>"
      "        <property name='homogeneous'>True</property>"
      "      </packing>"
      "    </child>"
      "    <child>"
      "      <object class='GtkToolButton' id='f_toolbar_zoom'>"
      "        <property name='visible'>True</property>"
      "        <property name='can_focus'>False</property>"
      "        <property name='tooltip_text' translatable='yes'>Zoom</property>"
      "        <property name='action_name'>figure.zoom</property>"
      "        <property name='icon_name'>zoom-in</property>"
      "      </object>"
      "      <packing>"
      "        <property name='expand'>False</property>"
      "        <property name='homogeneous'>True</property>"
      "      </packing>"
      "    </child>"
      "  </object>"
      "</interface>";

  try {
    m_refBuilder->add_from_string(ui_info);
  } catch (const Glib::Error& ex) {
    std::cerr << "Building toolbar failed: " << ex.what();
  }

  Gtk::Toolbar* toolbar = nullptr;
  m_refBuilder->get_widget("f_toolbar", toolbar);
  if (toolbar != nullptr) {
    attach(*toolbar, 0, 0, 1, 1);
  } else {
    g_warning("GtkToolbar not found");
  }

  attach(m_plt, 0, 1, 1, 1);
  m_plt.set_hexpand(true);
  m_plt.set_vexpand(true);

  show_all_children();
}

void Figure::plot(const v_cfloat& signal) { m_plt.set_y_values(signal); }

void Figure::on_toolbar_default() {
  m_plt.set_tool(pointer);
  std::cout << "Default Pointer" << '\n';
}

void Figure::on_toolbar_reset() {
  m_plt.reset_zoom();
  std::cout << "Reset Graph" << '\n';
}

void Figure::on_toolbar_zoom() {
  m_plt.set_tool(zoom);
  std::cout << "Zoom Tool" << '\n';
}
