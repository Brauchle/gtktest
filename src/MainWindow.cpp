#include "MainWindow.h"

#include <iostream>

MainWindow::MainWindow()
    : m_box_settings(Gtk::ORIENTATION_HORIZONTAL),
      m_Label1("Signal"),
      m_Label2("FFT"),
      m_Label3("PDF"),
      m_Label4("AKF"),
      m_Label5("Check"),
      m_label_fft_size("FFT Size"),
      m_Label_fs("Sample Rate"),
      m_Label_s("Samples"),
      m_Label_fs_v("-"),
      m_Label_s_v("-"),
      m_adjustment_fft_size( Gtk::Adjustment::create(1024.0, 128.0, 16384.0, 1024.0, 1024.0, 0.0) )
      //,
      //m_spinbutton_fft_size( m_adjustment_fft_size )
{
  set_title("Signal Analyzer");
  //set_border_width(10);
  // set_border_width(10);
  add(m_grid);

  //std::cout << m_adjustment_fft_size->get_lower() << " " << m_adjustment_fft_size->get_upper() << " " << m_adjustment_fft_size->get_value() << std::endl; ;
  m_spinbutton_fft_size.set_adjustment(m_adjustment_fft_size);
  //m_adjustment_fft_size->value_changed().connect()

  // Define how the actions are presented in the menus and toolbars:
  m_refBuilder = Gtk::Builder::create();

  Glib::ustring ui_info =
      "<!-- Generated with glade 3.18.3 -->"
      "<interface>"
      "  <requires lib='gtk+' version='3.4'/>"
      "  <object class='GtkToolbar' id='toolbar'>"
      "    <property name='visible'>True</property>"
      "    <property name='can_focus'>False</property>"
      "    <child>"
      "      <object class='GtkToolButton' id='toolbutton_new'>"
      "        <property name='visible'>True</property>"
      "        <property name='can_focus'>False</property>"
      "        <property name='tooltip_text' translatable='yes'>New "
      "Standard</property>"
      "        <property name='action_name'>app.open</property>"
      "        <property name='icon_name'>document-open</property>"
      "      </object>"
      "      <packing>"
      "        <property name='expand'>False</property>"
      "        <property name='homogeneous'>True</property>"
      "      </packing>"
      "    </child>"
      ""
      "    <child>"
      "     <object class='GtkMenuToolButton'>"
      "       <property name='label'>Transform</property>"
//      "       <property name='is-important'>True</property>"
      "       <child type='menu'>"
      "         <object class='GtkMenu'>"
      "           <property name='visible'>True</property>"
      "           <property name='can_focus'>False</property>"
      "           <child>"
      "             <object class='GtkMenuItem'>"
      "               <property name='visible'>True</property>"
      "               <property name='can_focus'>False</property>"
      "               <property name='label' translatable='yes'>None</property>"
      "               <property name='use_underline'>True</property>"
      "             </object>"
      "           </child>"
      "           <child>"
      "             <object class='GtkMenuItem'>"
      "               <property name='visible'>True</property>"
      "               <property name='can_focus'>False</property>"
      "               <property name='label' translatable='yes'>Quadratic</property>"
      "               <property name='use_underline'>True</property>"
      "             </object>"
      "           </child>"
      "           <child>"
      "             <object class='GtkMenuItem'>"
      "               <property name='visible'>True</property>"
      "               <property name='can_focus'>False</property>"
      "               <property name='label' translatable='yes'>Cubic</property>"
      "               <property name='use_underline'>True</property>"
      "             </object>"
      "           </child>"
      "         </object>"
      "       </child>"
      "     </object>"
      "    </child>"
      ""
      "    <child>"
      "      <object class='GtkToolItem' id='tool_spacer'>"
      "        <property name='visible'>True</property>"
      "        <property name='can_focus'>False</property>"
      "      </object>"
      "      <packing>"
      "        <property name='expand'>True</property>"
      "      </packing>"
      "    </child>"
      ""
      "    <child>"
      "      <object class='GtkToolButton' id='toolbutton_quit'>"
      "        <property name='visible'>True</property>"
      "        <property name='can_focus'>False</property>"
      "        <property name='tooltip_text' translatable='yes'>Quit</property>"
      "        <property name='action_name'>app.quit</property>"
      "        <property name='icon_name'>application-exit</property>"
      "      </object>"
      "      <packing>"
      "        <property name='expand'>False</property>"
      "        <property name='homogeneous'>True</property>"
      "      </packing>"
      "    </child>"
      "  </object>"
      "</interface>";

  try {
    m_refBuilder->add_from_string(ui_info);
  } catch (const Glib::Error& ex) {
    std::cerr << "Building menus and toolbar failed: " << ex.what();
  }

  Gtk::Toolbar* toolbar = nullptr;
  m_refBuilder->get_widget("toolbar", toolbar);
  if (toolbar == nullptr) {
    g_warning("GtkToolbar not found");
  } else {
    m_grid.attach(*toolbar, 0, 0, 1, 1);
  }

  // FFT SPIN BUTTON
  notebook_1.set_tab_pos(Gtk::POS_TOP);
  notebook_1.set_hexpand(true);
  notebook_1.set_vexpand(true);
  // Add the Notebook pages:
  // notebook_1.append_page(m_plt, m_Label1);
  notebook_1.append_page(m_fig_sig, m_Label1);
  notebook_1.append_page(m_fig_fft, m_Label2);
  notebook_1.append_page(m_fig_pdf, m_Label3);
  notebook_1.append_page(m_fig_akf, m_Label4);
  notebook_1.append_page(m_fig_check, m_Label5);

  // SETINGS
  m_frame.set_label("File Properties");
  m_frame.add(m_grid_2);
  m_grid_2.set_column_homogeneous(true);
  m_grid_2.attach(m_Label_fs, 0, 0, 1, 1);
  m_grid_2.attach(m_Label_fs_v, 1, 0, 1, 1);
  m_grid_2.attach(m_Label_s, 0, 1, 1, 1);
  m_grid_2.attach(m_Label_s_v, 1, 1, 1, 1);
  m_Label_fs.set_halign(Gtk::ALIGN_START);
  m_Label_s.set_halign(Gtk::ALIGN_START);

  m_frame_fft_settings.set_label("FFT Settings");
  m_frame_fft_settings.add(m_grid_fft_settings);
  m_grid_fft_settings.attach(m_label_fft_size, 0,0,1,1);
  m_grid_fft_settings.attach(m_spinbutton_fft_size, 1,0,1,1);

  m_box_settings.pack_start(m_frame);
  m_box_settings.pack_start(m_frame_fft_settings);

  m_grid.attach(notebook_1, 0, 1, 1, 1);

  m_grid.attach(m_box_settings, 0, 2, 1, 1);

  m_grid.set_row_spacing(10);
  m_grid.set_column_spacing(10);

  show_all_children();
}

void MainWindow::on_spinbutton_fft_size(){
  std::cout << " TEST " << std::endl;
}
