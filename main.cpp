#include <gtkmm.h>

#include "MainWindow.h"
#include "sigaapp.h"

int main(int argc, char *argv[])
{
  auto application = SigA::create();
  return application->run(argc, argv);
}
