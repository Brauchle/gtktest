#include "gtest/gtest.h"
#include "comp_test.h"
#include "convolution_test.h"


int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
